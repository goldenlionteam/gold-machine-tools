<?php get_header() ?>

<!--Banner section-->
<?php get_template_part("template-parts/home/banner")?>

<!--Search Cars section-->
<?php get_template_part("template-parts/home/about")?>

<!--Why us section-->
<?php get_template_part("template-parts/home/model")?>

<!--Featured Cars section-->
<?php get_template_part("template-parts/home/product")?>

<!--Customers section-->
<?php get_template_part("template-parts/home/testimonial")?>

<!--Brands section-->
<?php get_template_part("template-parts/home/contact")?>

<?php get_footer() ?>
