<?php get_header() ?>

<?php if (have_posts()): ?>
    <?php while (have_posts()): ?>
        <?php the_post(); ?>
        <section class="page-wrapper">
            <?php the_content() ?>
        </section>
    <?php endwhile; ?>
<?php endif; ?>

<?php get_footer() ?>
