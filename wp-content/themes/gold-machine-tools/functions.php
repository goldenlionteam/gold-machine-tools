<?php
/**
 * Twenty Twenty functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package WordPress
 * @subpackage gold-machine-tools
 */

/**
 * Table of Contents:
 * Theme Support
 * Register Styles
 * Register Scripts
 * Register Menus
 */

/**
 * Sets up theme defaults and registers support for various WordPress features.
 *
 * Note that this function is hooked into the after_setup_theme hook, which
 * runs before the init hook. The init hook is too late for some features, such
 * as indicating support for post thumbnails.
 */
function gold_machine_tools_theme_support() {

	// Add default posts and comments RSS feed links to head.
	add_theme_support( 'automatic-feed-links' );

	// Custom background color.
	add_theme_support(
		'custom-background',
		array(
			'default-color' => 'f5efe0',
		)
	);

	// Set content-width.
	global $content_width;
	if ( ! isset( $content_width ) ) {
		$content_width = 580;
	}

	/*
	 * Enable support for Post Thumbnails on posts and pages.
	 */
	add_theme_support( 'post-thumbnails' );

	// Set post thumbnail size.
	set_post_thumbnail_size( 1200, 9999 );

	// Add custom image size used in Cover Template.
	add_image_size( 'gold_machine_tools-fullscreen', 1980, 9999 );

	// Custom logo.
	$logo_width  = 120;
	$logo_height = 90;

	// If the retina setting is active, double the recommended width and height.
	if ( get_theme_mod( 'retina_logo', false ) ) {
		$logo_width  = floor( $logo_width * 2 );
		$logo_height = floor( $logo_height * 2 );
	}

	add_theme_support(
		'custom-logo',
		array(
			'height'      => $logo_height,
			'width'       => $logo_width,
			'flex-height' => true,
			'flex-width'  => true,
		)
	);

	/*
	 * Let WordPress manage the document title.
	 * By adding theme support, we declare that this theme does not use a
	 * hard-coded <title> tag in the document head, and expect WordPress to
	 * provide it for us.
	 */
	add_theme_support( 'title-tag' );

	/*
	 * Switch default core markup for search form, comment form, and comments
	 * to output valid HTML5.
	 */
	add_theme_support(
		'html5',
		array(
			'search-form',
			'comment-form',
			'comment-list',
			'gallery',
			'caption',
			'script',
			'style',
		)
	);

	/*
	 * Make theme available for translation.
	 * Translations can be filed in the /languages/ directory.
	 * If you're building a theme based on Twenty Twenty, use a find and replace
	 * to change 'gold_machine_tools' to the name of your theme in all the template files.
	 */
	load_theme_textdomain( 'gold_machine_tools' );

	// Add support for full and wide align images.
	add_theme_support( 'align-wide' );

	// Add support for responsive embeds.
	add_theme_support( 'responsive-embeds' );

	// Add theme support for selective refresh for widgets.
	add_theme_support( 'customize-selective-refresh-widgets' );
}

add_action( 'after_setup_theme', 'gold_machine_tools_theme_support' );


/**
 * Register and Enqueue Styles.
 */
 
function gold_machine_tools_enqueue_style() {
	$theme_version = wp_get_theme()->get( 'Version' );
	wp_enqueue_style( 'gold_machine_tools-font-awesome', get_theme_file_uri( '/assets/css/font-awesome.min.css' ), $theme_version, false );
    wp_enqueue_style( 'theme-font1', 'https://fonts.googleapis.com/css?family=Montserrat:300,400,500,600,700,800,900', $theme_version, false );
    wp_enqueue_style( 'theme-font2', 'https://fonts.googleapis.com/css2?family=Teko:wght@500;600;700&display=swap', $theme_version, false );
	wp_enqueue_style( 'theme-stylesheet', get_stylesheet_uri(), $theme_version, false );
	wp_enqueue_style( 'gold_machine_tools-bootstrap-style', get_theme_file_uri( '/assets/css/bootstrap.min.css' ), $theme_version, false );
    wp_enqueue_style( 'gold_machine_tools-owl-style', get_theme_file_uri( '/assets/css/owl.carousel.min.css' ), $theme_version, false );
    wp_enqueue_style( 'gold_machine_tools-custom-style', get_theme_file_uri( '/assets/css/custom.css' ), $theme_version, false );
    wp_enqueue_style( 'gold_machine_tools-selcet2-style', get_theme_file_uri( '/assets/css/select2.min.css' ), $theme_version, false );
    wp_enqueue_style( 'gold_machine_tools-responsive-style', get_theme_file_uri( '/assets/css/responsive.css' ), $theme_version, false );
}
add_action( 'wp_enqueue_scripts', 'gold_machine_tools_enqueue_style' );

/**
 * Register and Enqueue Scripts.
 */

function gold_machine_tools_enqueue_script() {
	$theme_version = wp_get_theme()->get( 'Version' );
    wp_enqueue_script( 'gold_machine_tools-jquery-js', get_theme_file_uri( '/assets/js/jquery-3.2.1.min.js' ), array(), $theme_version, false );
    wp_enqueue_script( 'gold_machine_tools-bootstrap-js', get_theme_file_uri( '/assets/js/bootstrap.min.js' ), array(), $theme_version, false );
    wp_enqueue_script( 'gold_machine_tools-owl-js', get_theme_file_uri( '/assets/js/owl.carousel.min.js' ), array(), $theme_version, false );
    wp_enqueue_script( 'gold_machine_tools-custom-js', get_theme_file_uri( '/assets/js/custom.js' ), array(), $theme_version, false );
    wp_enqueue_script( 'gold_machine_tools-select2-js', get_theme_file_uri( '/assets/js/select2.min.js' ), array(), $theme_version, false );
}
add_action( 'wp_enqueue_scripts', 'gold_machine_tools_enqueue_script' );


/**
 * Register navigation menus uses wp_nav_menu in five places.
 */
function gold_machine_tools_menus() {

	$locations = array(
		'top'   => __( 'Top Menu', 'gold_machine_tools' ),
		'footer'   => __( 'Footer Menu', 'gold_machine_tools' ),
		'service'   => __( 'Service Menu', 'gold_machine_tools' ),
	);

	register_nav_menus( $locations );
}

add_action( 'init', 'gold_machine_tools_menus' );


if ( ! function_exists( 'wp_body_open' ) ) {

	/**
	 * Shim for wp_body_open, ensuring backward compatibility with versions of WordPress older than 5.2.
	 */
	function wp_body_open() {
		do_action( 'wp_body_open' );
	}
}

// Hide admin bar from front end
add_filter('show_admin_bar', '__return_false');

// Add option page and sub page
if( function_exists('acf_add_options_page') ) {
	acf_add_options_page(array(
		'page_title' 	=> 'General Settings',
		'menu_title'	=> 'General Settings',
		'menu_slug' 	=> 'theme-general-settings',
		'capability'	=> 'edit_posts',
		'redirect'		=> false
	));
	acf_add_options_sub_page(array(
		'page_title' 	=> 'Theme Header Settings',
		'menu_title'	=> 'Header',
		'parent_slug'	=> 'theme-general-settings',
	));
	acf_add_options_sub_page(array(
		'page_title' 	=> 'Theme Footer Settings',
		'menu_title'	=> 'Footer',
		'parent_slug'	=> 'theme-general-settings',
	));
}

/**
 * Allow svg file upload
 */
function cc_mime_types($mimes) {
	$mimes['svg'] = 'image/svg+xml';
	return $mimes;
}
add_filter('upload_mimes', 'cc_mime_types');


/**
 * Allow webp file upload
 */
function webp_upload_mimes( $existing_mimes ) {
	$existing_mimes['webp'] = 'image/webp';
	return $existing_mimes;
}
add_filter( 'mime_types', 'webp_upload_mimes' );


/*
CRM Integration
*/

function submit_data() {
	
	$refresh_token = "1000.4b0c1b7fb3f97aeeadb668f39f4fe1cb.2283444d5edfd1bdfcc3acf91274556e";
	$client_id = "1000.R4FTIB1YR2CJD0Q8QRWJZ0KRYMM2KH";
	$client_secreate = "403de9f88c19b1fdb837f5560656e924f2c5e509f8";

	/*GENERATE ACCESS TOKEN*/
	$curl = curl_init();
		
	$curl_header = array(
	CURLOPT_URL => "https://accounts.zoho.com/oauth/v2/token?refresh_token=".$refresh_token."&client_id=".$client_id."&client_secret=".$client_secreate."&grant_type=refresh_token",
	CURLOPT_RETURNTRANSFER => true,
	CURLOPT_ENCODING => "",
	CURLOPT_MAXREDIRS => 10,
	CURLOPT_TIMEOUT => 8,
	CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
	CURLOPT_CUSTOMREQUEST => "POST",
	CURLOPT_HTTPHEADER => array(
		"cache-control: no-cache"
	)
	);

	curl_setopt_array(
	$curl, 
	$curl_header
	);

	$token = curl_exec($curl);
	$err = curl_error($curl);

	if ($err) {
	echo $err;
	die;
	} else {
	$token = json_decode($token);
	}

	$create_lead_paylode1 = '{
		"data": [
			{
			"Owner"                                     : "1829555000000095003",
			"Enquiry_Type"                         		: "' . $_POST['type'] . '",
			"Company"                              		: "' . $_POST['company'] . '",
			"First_Name"                                : "' . $_POST['fname'] . '",
			"Last_Name"                                 : "' . $_POST['lname'] . '",
			"Email"                                     : "' . $_POST['email'] . '",
			"Mobile"                                    : "' . $_POST['mobile'] . '",
			"Phone"                                    	: "' . $_POST['phone'] . '",
			"Description"                               : "' . $_POST['description'] . '",
			"Main_Product_Group"                       	: "' . $_POST['product_group'] . '",
			"Lead_Source"                       		: "Website",
			"Lead_Status"                       		: "Not Contacted",
			}
		],
		"trigger":[
			"workflow"
		]
	}';

	$create_lead_curl1 = curl_init();
	curl_setopt_array($create_lead_curl1, array(
	CURLOPT_URL => "https://www.zohoapis.com/crm/v2/Leads",
	CURLOPT_RETURNTRANSFER => true,
	CURLOPT_ENCODING => "",
	CURLOPT_MAXREDIRS => 10,
	CURLOPT_TIMEOUT => 8,
	CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
	CURLOPT_CUSTOMREQUEST => "POST",
	CURLOPT_POSTFIELDS => $create_lead_paylode1,
	CURLOPT_HTTPHEADER => array(
		"Authorization: Zoho-oauthtoken " . $token->access_token,
		"Content-Type: application/json",
	),
	) );

	$lead_response1 = curl_exec($create_lead_curl1);
	$contac_err1 = curl_error($create_lead_curl1);
	curl_close($create_lead_curl1);

	$lead_response_json1 = json_decode( $lead_response1, true );

	$LEAD = $lead_response_json1['data']['0']['details']['id'];

	if($LEAD) :
		echo $_POST['id'];
	endif;


	wp_die();
}

add_action('wp_ajax_submit_data', 'submit_data');
add_action('wp_ajax_nopriv_submit_data', 'submit_data');

?>