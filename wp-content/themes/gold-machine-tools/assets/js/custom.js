function onScroll(s) {
    var e = $(document).scrollTop();
    $(".menu-top-menu-container a").each(function() {
        var s = $(this),
        a = $(s.attr("href"));
        a.position().top - 100 <= e && a.position().top + a.height() > e ? ($(".menu-top-menu-container ul li").removeClass("active"), s.parent().addClass("active")) : e > 43 && a.position().top - 500 <= e && a.position().top + a.height() > e ? ($(".menu-top-menu-container ul li").removeClass("active"), s.parent().addClass("active")) : s.parent().removeClass("active");
    })
}
$( document ).ready(function() {
    $('select').select2({});
    $('select#typ1').select2({
        minimumResultsForSearch: -1
    });
    $(".contact-section ul.nav li.nav-item a").click(function(){
        $(".contact-section #f2 .select2-container").css("width", "540px");
    });
    $(".menu-main-menu-container ul li").click(function(){
        // $(this).addClass('active').siblings().removeClass('active');
         $('.navbar-collapse').removeClass('show').addClass('hide');
     });

    // for hamburger menu cross
    $('.hamburger-button').on('click', function () {
        $('.animated-close').toggleClass('open');
      });
    
    $('.hamburger-close').on('click', function (e) {
        $('.navbar-collapse').removeClass('show');
        $('.animated-close').removeClass('open');
    });

    $('#top-menu li a').on('click', function () {
        $('.animated-close').removeClass('open');
    });

    $('.testi-carousel').owlCarousel({
        minSlides: 1,
        maxSlides: 1,
        slideMargin: 30,
        infiniteLoop: !1,
        hideControlOnEnd: !0,
        speed: 1800,
        adaptiveHeight: true,
        adaptiveHeightSpeed: 1800,
        dots: true,
        items: 2,
        responsive:{
            0:{
                items:1,
                nav:true
            },
            600:{
                items:1,
                nav:false
            },
            1000:{
                items:1,
                nav:true,
                loop:false
            }
        }
    });

    $(document).on("scroll", onScroll), $('.navbar-nav a[href^="#"]').on("click", function(s) {
        s.preventDefault(), 
        $(document).off("scroll"), $(".navbar-nav a").each(function() {
            $(this).removeClass("active")
        }), $(this).addClass("active");
        var e = this.hash;
        $target = $(e), $("html, body").stop().animate({
            scrollTop: $target.offset().top    
        }, 500, "swing", function() {
            window.location.hash = e, $(document).on("scroll", onScroll)
        })
    });


    $(".contact-section #form1, .contact-section #form2").click(function(){
        $(this).attr("disabled", "disabled");
        $(this).animate({"border-radius": "50%", "text-indent": "-9999999px", "width": "50px", "height": "50px", "padding": "0"},200);
        $(this).find(".load").animate({"opacity": "1"},200);

        $.ajax({
            type: 'POST',
            url: ajaxurl,
            data: {
                'action'            : 'submit_data',
                'id'                :  $(this).data('id'),
                'fname'             :  $("#fn"+$(this).data('id')).val(),
                'lname'             :  $("#ln"+$(this).data('id')).val(),
                'email'             :  $("#eml"+$(this).data('id')).val(),
                'company'           :  $("#com"+$(this).data('id')).val(),
                'type'              :  $("#typ"+$(this).data('id')).val(),
                'product_group'     :  $("#pro"+$(this).data('id')).val(),
                'mobile'            :  $("#mob"+$(this).data('id')).val(),
                'phone'             :  $("#phn"+$(this).data('id')).val(),
                'description'       :  $("#msg"+$(this).data('id')).val(),
            }, success: function (result) {
                if(result != "") {
                    $("#form"+result+" .load").css("opacity", "0");
                    $("#form"+result).animate({"width": "200px", "height": "50px", "padding": "12px 40px", "border-radius": "5px", "text-indent": "0"},200);
                    $("#form"+result+" span").text("Thank You");
                    $("#form"+result+" .suc").removeClass("d-none");

                    $("#fn"+result).val("");
                    $("#ln"+result).val("");
                    $("#eml"+result).val("");
                    $("#com"+result).val("");
                    $("#typ"+result).val("").trigger('change');
                    $("#pro"+result).val("").trigger('change');
                    $("#mob"+result).val("");
                    $("#phn"+result).val("");
                    $("#msg"+result).val("");
                    setTimeout(function() {
                        $("#form"+result).animate({"width": "200px", "height": "50px", "padding": "12px 40px", "border-radius": "5px", "text-indent": "0"}, 200);
                        $("#form"+result+" span").text("CONTACT US");
                        $("#form"+result+" .suc").addClass("d-none");
                    }, 5000);
                }
            },
            error: function () {
                alert("error");
            }
        });
    });
     
 });

// $(window).scroll(function() {
//     var scrollDistance = $(window).scrollTop();
    
// if(scrollDistance>0 && scrollDistance<3200)
// {
//     $('.page-section').each(function(i) {
//         if ($(this).position().top <= scrollDistance) {
//                 $('ul.header-nav li.active').removeClass('active');
//                 $('ul.header-nav li').eq(i).addClass('active');
//             }
//     });
// }
// else
// {
//     $('ul.header-nav li.active').removeClass('active');
// }

//     // Assign active class to nav links while scolling
    
// }).scroll();


