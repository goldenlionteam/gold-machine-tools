<?php
/**
 * The template for displaying the footer
 *
 * Contains the opening of the #site-footer div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package WordPress
 * @subpackage gold-machine-tools
 */

?>
<?php 
$com_details = get_field('company_details', 'option');
$social_details = get_field('social_link', 'option');
?>
<footer class="text-center">
    <div class="py-100 bg-dark-gray">
      <div class="container">
				<div class="row justify-content-center text-center">
					<div class="col-lg-8 col-xl-7 col-md-10 my-5">
						<h4 class="pb-2 fw-7 m-0 color-white text-center">Join our Newsletter</h4>
					<!--Zoho Campaigns Web-Optin Form's Header Code Starts Here-->

<script type="text/javascript" src="https://wrsn.maillist-manage.com/js/optin.min.js" onload="setupSF('sf35015ad93bf6c131a8fa6f38bbb37d0c64ea8c0b48e713d7','ZCFORMVIEW',false,'light',false,'0')"></script>
<script type="text/javascript">
	function runOnFormSubmit_sf35015ad93bf6c131a8fa6f38bbb37d0c64ea8c0b48e713d7(th){
		/*Before submit, if you want to trigger your event, "include your code here"*/
	};
</script>

<!--Zoho Campaigns Web-Optin Form's Header Code Ends Here--><!--Zoho Campaigns Web-Optin Form Starts Here-->

<div id="sf35015ad93bf6c131a8fa6f38bbb37d0c64ea8c0b48e713d7" data-type="signupform" style="opacity: 1;">
	<div id="customForm">
		<div class="quick_form_8_css" name="SIGNUP_BODY">
			<div>
				<!-- <div style="font-size: 14px; font-family: &quot;Arial&quot;; font-weight: bold; color: rgb(136, 136, 136); text-align: left; padding: 10px 20px 5px; width: 100%; display: block" id="SIGNUP_HEADING">Join Our Newsletter</div> -->
				<div>
					<div id="Zc_SignupSuccess" style="display:none">
						<table width="100%" cellpadding="0" cellspacing="0" border="0">
							<tbody>
								<tr>
									<td width="10%">
										<img class="successicon" src="https://wrsn.maillist-manage.com/images/challangeiconenable.jpg" align="absmiddle">
									</td>
									<td>
										<span id="signupSuccessMsg" class="color-white">&nbsp;&nbsp;Thank you for Signing Up</span>
									</td>
								</tr>
							</tbody>
						</table>
					</div>
				</div>
				<form method="POST" id="zcampaignOptinForm" action="https://maillist-manage.com/weboptin.zc" target="_zcSignup" class="news-form my-2">
					<div style="display: none;position: absolute;top: 110px;left: 50%;transform: translate(-50%);width: 100%;color: #f70000;" id="errorMsgDiv">Please correct the marked field(s) below.</div>
					<div class="SIGNUP_FLD">
						<div id="Zc_SignupSuccess" style="display: none">
							<div>
								<img class="successicon" src="https://campaigns.zoho.com/images/challangeiconenable.jpg" style="width: 20px">
							</div>
							<div>
								<span id="signupSuccessMsg"></span>
							</div>
						</div>
						<input type="text" class="form-control" placeholder="Email" changeitem="SIGNUP_FORM_FIELD" name="CONTACT_EMAIL" id="EMBED_FORM_EMAIL_LABEL">
					</div>
					<div class="SIGNUP_FLD">
						<input type="button" class="btn btn-yellow" name="SIGNUP_SUBMIT_BUTTON" id="zcWebOptin" value="Join Now">
					</div>
					<input type="hidden" id="fieldBorder" value="">
					<input type="hidden" id="submitType" name="submitType" value="optinCustomView">
					<input type="hidden" id="emailReportId" name="emailReportId" value="">
					<input type="hidden" id="formType" name="formType" value="QuickForm">
					<input type="hidden" name="zx" id="cmpZuid" value="1943cf23">
					<input type="hidden" name="zcvers" value="3.0">
					<input type="hidden" name="oldListIds" id="allCheckedListIds" value="">
					<input type="hidden" id="mode" name="mode" value="OptinCreateView">
					<input type="hidden" id="zcld" name="zcld" value="1323205f4e1b1361">
					<input type="hidden" id="document_domain" value="">
					<input type="hidden" id="zc_Url" value="wrsn.maillist-manage.com">
					<input type="hidden" id="new_optin_response_in" value="0">
					<input type="hidden" id="duplicate_optin_response_in" value="0">
					<input type="hidden" name="zc_trackCode" id="zc_trackCode" value="ZCFORMVIEW">
					<input type="hidden" id="zc_formIx" name="zc_formIx" value="35015ad93bf6c131a8fa6f38bbb37d0c64ea8c0b48e713d7">
					<input type="hidden" id="viewFrom" value="URL_ACTION">
					<span style="display: none" id="dt_CONTACT_EMAIL">1,true,6,Contact Email,2</span>
				</form>
			</div>
		</div>
	</div>
	<img src="https://wrsn.maillist-manage.com/images/spacer.gif" id="refImage" onload="referenceSetter(this)" style="display:none;">
</div>
<input type="hidden" id="signupFormType" value="QuickForm_Horizontal">
<div id="zcOptinOverLay" oncontextmenu="return false" style="display:none;text-align: center; background-color: rgb(0, 0, 0); opacity: 0.5; z-index: 100; position: fixed; width: 100%; top: 0px; left: 0px; height: 988px;"></div>
<div id="zcOptinSuccessPopup" style="display:none;z-index: 9999;width: 800px; height: 40%;top: 84px;position: fixed; left: 26%;background-color: #FFFFFF;border-color: #E6E6E6; border-style: solid; border-width: 1px;  box-shadow: 0 1px 10px #424242;padding: 35px;">
	<span style="position: absolute;top: -16px;right:-14px;z-index:99999;cursor: pointer;" id="closeSuccess">
		<img src="https://wrsn.maillist-manage.com/images/videoclose.png">
	</span>
	<div id="zcOptinSuccessPanel"></div>
</div>

<!--Zoho Campaigns Web-Optin Form Ends Here-->
					</div>
				</div>
        <div class="row justify-content-center text-left">
		  <?php if($com_details): ?>
          <div class="col-lg-9 col-md-6 mb-4 mb-lg-0">
            <ul class="footer-link float-left">
			<?php if($com_details['address']): ?>
			  <li class="fw-4 d-flex pb-3"><div class="color-yellow float-left w-120"><i class="fa fa-map-marker mx-2"></i> Address : </div><div class="color-gray float-left"><?php echo $com_details['address']; ?></div></li>
			<?php endif; 
			if($com_details['phone']):
			?>
			  <li class="fw-4 d-flex pb-3"><div class="color-yellow float-left w-120"><i class="fa fa-phone mx-2"></i> Phone : </div><div class="color-gray float-left"><a class="color-gray" href="tel:<?php echo $com_details['phone']; ?>"><?php echo $com_details['phone']; ?></a></div></li>
			<?php endif; 
			if($com_details['fax']):
			?> 
			  <li class="fw-4 d-flex pb-3"><div class="color-yellow float-left w-120"><i class="fa fa-fax mx-2"></i> Fax : </div><div class="color-gray float-left"><?php echo $com_details['fax']; ?></div></li>
			<?php endif; 
			if($com_details['email']):
			?>
			  <li class="fw-4 d-flex pb-3"><div class="color-yellow float-left w-120"><i class="fa fa-envelope mx-2"></i> E-Mail : </div><div class="color-gray float-left"><a class="color-gray" href="mailto:<?php echo $com_details['email']; ?>"><?php echo $com_details['email']; ?></a></div></li>
			<?php endif; ?>
            </ul>
		  </div>
		  <?php endif; 
		  if($social_details):
		  ?>
          <div class="col-lg-3 col-md-6 mb-4 mb-lg-0">
            <p class="fw-4 mb-3 color-gray text-center follow">Follow us on</p>
            <ul class="social-link text-center">
			<?php if($social_details['facebook']): ?>
				<li><a target="blank" href="<?php echo $social_details['facebook']; ?>"><i class="fa fa-facebook"></i></a></li>
			<?php endif; 
			if($social_details['linkedin']):
			?>
				<li><a target="blank" href="<?php echo $social_details['linkedin']; ?>"><i class="fa fa-linkedin"></i></a></li>
			<?php endif; 
			if($social_details['twitter']):
			?>
				<li><a target="blank" href="<?php echo $social_details['twitter']; ?>"><i class="fa fa-twitter"></i></a></li>
			<?php endif; ?>
            </ul>
		  </div>
		<?php endif; ?>
        </div>
      </div>
    </div>
    <div class="copy-area py-4">
      <div class="container">
        <div class="row">
          <div class="col">
            <span>Copyright - © <?php echo date('Y'); ?> Gold Machine Tools | All Rights Reserved.</span>
          </div>
        </div>
      </div>
    </div>
  </footer>
