<?php
/**
 * Header file for the Twenty Twenty WordPress default theme.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package WordPress
 * @subpackage gold-machine-tools
 */

?><!DOCTYPE html>

<html class="no-js" <?php language_attributes(); ?>>

	<head>

		<meta charset="<?php bloginfo( 'charset' ); ?>">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<meta http-equiv="X-UA-Compatible" content="ie=edge">
		<?php wp_head(); ?>

		<!-- favicon -->
		<link rel="shortcut icon" type="image/x-icon" href="<?php echo get_template_directory_uri(); ?>/assets/images/favicon.ico" />

		<!-- SalesIQ Start-->
		<script type="text/javascript">
		var $zoho=$zoho || {};$zoho.salesiq = $zoho.salesiq || {widgetcode:"ee7b8233f908cc287b531174c07f872ebf67b48960d7add58dfd53497e78290c6f3545a588a17aedb379259402759322", values:{},ready:function(){}};var d=document;s=d.createElement("script");s.type="text/javascript";s.id="zsiqscript";s.defer=true;s.src="https://salesiq.zoho.com/widget";t=d.getElementsByTagName("script")[0];t.parentNode.insertBefore(s,t);d.write("<div id='zsiqwidget'></div>");
		</script>
		<!-- SalesIQ End-->

	</head>

	<body <?php body_class(); ?>>

		<?php
		wp_body_open();
		?>

		<header class="fixed-top py-4">
			<div class="container full_container_responsive">
				<div class="row align-items-center">
				<div class="col">
					<nav class="navbar navbar-expand-lg navbar-light p-0 m-0 border-0">
						<?php $logo = get_field('logo', 'option');
						if(!empty($logo)):
						?>
						<div class="navbar-brand p-0">
							<a class="logo d-block" href="<?php echo site_url(); ?>">
							<img src="<?php echo $logo['url']; ?>" alt="<?php echo $logo['alt']; ?>" width="246px" height="55px">
							</a>
						</div>
						<?php endif; ?>
						<button class="navbar-toggler hamburger-button" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
							<div class="animated-close"><span></span><span></span><span></span><span></span></div>
						</button>
						
						<div class="collapse navbar-collapse flex-end pr-0" id="navbarSupportedContent">
							
							<div class="menu-top-menu-container">
								<ul id="top-menu" class="navbar-nav header-nav">
									<li><a href="#about-section">About Us</a></li>
									<li><a href="#product-section">Products</a></li>
									<li><a href="#contact-section">Contact Us</a></li>
									<li><a href="https://manufacturingcrm1.zohocreatorportal.com" target="blank">Portal Login</a></li>
								</ul>
							</div>                      
							
						</div>
						</nav>
					</div>
				</div>
			</div>
		</header>
		<!-- #site-header -->
