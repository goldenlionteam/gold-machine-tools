<section class="page-section easy-steps py-100">
    <div class="container">
      <div class="row justify-content-center text-center">
        <?php if(get_field('model_heading')): ?>
        <div class="col-12 title-wrap text-center mb-5">
          <h2 class="pb-3 m-0"><?php the_field('model_heading'); ?></h2>
        </div>
        <?php endif; 
        if(have_rows('model_list')):
        while(have_rows('model_list')): the_row();
        ?>
        <div class="col-lg-4 col-md-6 mb-5">
          <div class="stape-panel h-100">
            <span class="font-14 color-dark-gray d-block fw-6 mb-3">Step <?php echo get_row_index(); ?></span>
            <?php $model_img = get_sub_field('icon');
            if(!empty($model_img)):
            ?>
            <div class="img-box"> 
              <img class="img-fluid" src="<?php echo $model_img['url']; ?>" alt="<?php echo $model_img['alt']; ?>">
            </div>
            <?php endif;
            if(get_sub_field('title')):
            ?>
            <h5 class="fw-7 mb-3"><?php the_sub_field('title'); ?></h5>
            <?php endif;
            if(get_sub_field('description')):
            ?>
            <p class="m-0"><?php the_sub_field('description'); ?></p>
            <?php endif; ?>
          </div>
        </div>
        <?php endwhile; endif; ?>
        <div class="col-12 text-center">
          <a href="#contact-section" class="btn btn-yellow">Contact Us</a>
        </div>
        
      </div>
    </div>
  </section>