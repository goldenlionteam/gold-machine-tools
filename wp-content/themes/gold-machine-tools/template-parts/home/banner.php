<section id="banner-section" class="banner page-section">
    <div class="container">
        <?php $banner_img = get_field('banner_image'); ?>
        <div class="row align-items-center position-relative" style="background-image:url(<?php echo $banner_img['url']; ?>);">
        <div class="banner-back-overlay"></div>
        <div class="col-xl-6 col-lg-6">
            <?php if(get_field('banner_heading')): ?>
            <h1 class="mb-4"><?php the_field('banner_heading'); ?></h1>
            <?php endif; ?>
            <a href="#contact-section" class="btn btn-yellow">Contact Us</a>
        </div>
        </div>
    </div>
</section>