<section class="bg-light-yellow py-100 testi-section page-section">
    <div class="container">
      <div class="row justify-content-center">
        <?php if(get_field('testimonials_heading')): ?>
        <div class="col-12 title-wrap text-center mb-5">
          <h2 class="pb-3 m-0"><?php the_field('testimonials_heading'); ?></h2>
        </div>
        <?php endif; ?>
        <div class="col-xl-10 col-lg-10">
          <div class="testi-carousel owl-carousel owl-theme">
            <?php 
            if(have_rows('testimonials_list')):
            while(have_rows('testimonials_list')): the_row();
            $testi_img = get_sub_field('image');
            ?>
            <div class="item text-center">
              <?php if(!empty($testi_img)): ?>
              <div class="author-img"><img src="<?php echo $testi_img['url']; ?>" class="img-fluid" alt="<?php echo $testi_img['alt']; ?>"></div>
              <?php endif; 
              if(get_sub_field('content')):
              ?>
              <p class="mb-4 text-left"><?php echo strip_tags(get_sub_field('content')); ?></p>
              <?php endif; 
              if(get_sub_field('customer_name')):
              ?>
              <div class="author-name fw-6 color-black"><?php the_sub_field('customer_name'); ?></div>
              <?php endif; 
              if(get_sub_field('company_name')):
              ?>
              <div class="author-degi font-14"><?php the_sub_field('company_name'); ?></div>
              <?php endif; ?>
            </div>
            <?php 
            endwhile;
            endif;
            ?>
          </div>
        </div>
      </div>
    </div>
  </section>