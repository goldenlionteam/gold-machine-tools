<section id="product-section" class="page-section py-100">
    <div class="container">
        <div class="row justify-content-center">
          <?php if(get_field('product_heading')): ?>
          <div class="col-12 title-wrap text-center mb-5">
            <h2 class="pb-3 m-0"><?php the_field('product_heading'); ?></h2>
          </div>
          <?php endif;
          if(have_rows('product_list')):
          while(have_rows('product_list')): the_row();
          ?>

          <div class="col-lg-3 col-md-6 mb-5">
            <div class="catalog">
              <?php if(get_sub_field('rate')): ?>
              <div class="percentage font-14"><?php the_sub_field('rate'); ?></div>
              <?php endif;
              $pro_img = get_sub_field('image');
              if(!empty($pro_img)):
              ?>
              <img src="<?php echo $pro_img['url']; ?>" class="img-fluid" alt="<?php echo $pro_img['alt']; ?>">
              <?php endif; 
              if(get_sub_field('name')):
              ?>
              <div class="bottom-text"><?php the_sub_field('name'); ?></div>
              <?php endif; ?>
            </div>
          </div>
          <?php endwhile;
          endif;
          ?>
      </div>
    </div>
  </section>