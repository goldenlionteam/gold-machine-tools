<section id="about-section" class="page-section py-100">
    <div class="container">
      <div class="row align-items-center">
        <div class="col-lg-12 mb-5 mb-lg-0 text-center">
          <?php if(get_field('about_heading')): ?>
          <h2><?php the_field('about_heading'); ?></h2>
          <?php endif;
          if(get_field('about_description')):
            the_field('about_description');
          endif; ?>
        </div>
      </div>
      <?php if(have_rows('about_list')): ?>
      <div class="row align-items-center mt-5">
        <?php while(have_rows('about_list')): the_row(); ?>
        <div class="col-lg-4 text-center mb-5">
          <?php $img = get_sub_field('image'); 
            if(!empty($img)):
          ?>
          <div class="tech-img">
            <img src="<?php echo $img['url']; ?>" class="img-fluid" alt="<?php echo $img['alt']; ?>">
          </div>
          <?php endif;
          if(get_sub_field('title')):
          ?>
          <h3 class="mt-4"><?php the_sub_field('title'); ?></h3>
          <?php endif;
          if(get_sub_field('description')):
            the_sub_field('description');
          endif;
          ?>
        </div>
        <?php endwhile; ?>
      </div>
      <?php endif; ?>
    </div>
  </section>