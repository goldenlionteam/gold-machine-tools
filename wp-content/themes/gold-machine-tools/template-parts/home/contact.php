<?php 
$refresh_token = "1000.4b0c1b7fb3f97aeeadb668f39f4fe1cb.2283444d5edfd1bdfcc3acf91274556e";
$client_id = "1000.R4FTIB1YR2CJD0Q8QRWJZ0KRYMM2KH";
$client_secreate = "403de9f88c19b1fdb837f5560656e924f2c5e509f8";

/*GENERATE ACCESS TOKEN*/
$curl = curl_init();
	
$curl_header = array(
  CURLOPT_URL => "https://accounts.zoho.com/oauth/v2/token?refresh_token=".$refresh_token."&client_id=".$client_id."&client_secret=".$client_secreate."&grant_type=refresh_token",
  CURLOPT_RETURNTRANSFER => true,
  CURLOPT_ENCODING => "",
  CURLOPT_MAXREDIRS => 10,
  CURLOPT_TIMEOUT => 8,
  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
  CURLOPT_CUSTOMREQUEST => "POST",
  CURLOPT_HTTPHEADER => array(
    "cache-control: no-cache"
  )
);

curl_setopt_array(
  $curl, 
  $curl_header
);

$token = curl_exec($curl);
$err = curl_error($curl);

if ($err) {
  echo $err;
  die;
} else {
  $token = json_decode($token);
}

$search_contact_uri = "https://www.zohoapis.com/crm/v2/Product_Group";
$search_contact_curl = curl_init();
  $curl_header = array(
      CURLOPT_URL => $search_contact_uri,
      CURLOPT_RETURNTRANSFER => true,
      CURLOPT_ENCODING => "",
      CURLOPT_MAXREDIRS => 10,
      CURLOPT_TIMEOUT => 8,
      CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
      CURLOPT_CUSTOMREQUEST => 'GET',
    );
  $curl_header[CURLOPT_HTTPHEADER] =  array(
    "Authorization: Zoho-oauthtoken ".$token->access_token,
    "Content-Type: application/json",
    "Cache-Control: no-cache"	
  );
  curl_setopt_array(
    $curl, 
    $curl_header
  );
      
  $search_contact_response = curl_exec($curl);
  $search_contact_err = curl_error($curl);

if ($search_contact_err) {
  echo $search_contact_err;
  die;
} else {
  $search_contact_response_json = json_decode($search_contact_response);
}
curl_close($search_contact_curl);

?>
<section class="page-section contact-section py-100 cover position-relative" id="contact-section">
    <div class="contact-back-overlay"></div>
    <div class="container">
      <div class="row align-items-center justify-content-center">
        <div class="col-md-6 offset-md-6">
        <h2 class="pb-2 mb-4">Contact Us</h2>
          <ul class="nav nav-tabs mb-5" id="myTab" role="tablist">
              <li class="nav-item" role="presentation">
                <a class="nav-link px-0 py-0 active" id="car-brand-tab" data-toggle="tab" href="#car-brand" role="tab" aria-controls="car-brand" aria-selected="true">Distributor / Dealer</a>
              </li>
              <li class="nav-item" role="presentation">
                <a class="nav-link px-0 py-0 mx-5" id="car-type-tab" data-toggle="tab" href="#car-type" role="tab" aria-controls="car-type" aria-selected="false">End Customer</a>
              </li>
          </ul>
          <div class="tab-content" id="myTabContent">
            <div class="tab-pane fade show active" id="car-brand" role="tabpanel" aria-labelledby="car-brand-tab">
              <form id="f1" method="POST" class="contact-form">
                <div class="row">
                  <div class="col form-group">
                    <label class="requird" for="">First name</label>
                    <input type="text" id="fn1" name="fn1" class="form-control" aria-required="true" onkeyup="validate()">
                  </div>
                  <div class="col form-group">
                    <label class="requird" for="">Last name</label>
                    <input type="text" id="ln1" name="ln1" class="form-control" aria-required="true" onkeyup="validate()">
                  </div>
                </div>
                <div class="row">
                  <div class="col form-group">
                    <label class="requird" for="">Email</label>
                    <input type="email" id="eml1" name="eml1" class="form-control" aria-required="true" onkeyup="validate()">
                  </div>
                </div>
                <div class="row">
                  <div class="col form-group">
                    <label class="requird" for="">Company</label>
                    <input type="text" id="com1" name="com1" class="form-control" aria-required="true" onkeyup="validate()">
                  </div>
                  <div class="col form-group">
                    <label class="requird" for="">Type</label>
                    <select class="col form-control" id="typ1" name="typ1" onchange="validate()">
                      <option value="">Choose Type</option>
                      <option value="Distributor">Distributor</option>
                      <option value="Dealer">Dealer</option>
                    </select>
                  </div>
                </div>
                <div class="row">
                  <div class="col-12 form-group">
                    <label class="requird" for="">Product Group</label>
                    <select class="form-control" id="pro1" name="pro1" onchange="validate()">
                      <option value="">Choose Product Group</option>
                      <?php foreach($search_contact_response_json->data as $pro_grp):
                      ?>
                      <option value="<?php echo $pro_grp->id; ?>"><?php echo $pro_grp->Name; ?></option>
                      <?php endforeach; ?>
                    </select>
                  </div>
                </div>
                <div class="row">
                  <div class="col form-group">
                    <label class="requird" for="">Mobile</label>
                    <input type="text" id="mob1" name="mob1" class="form-control" aria-required="true" onkeyup="validate()">
                  </div>
                  <div class="col form-group">
                    <label for="">Phone</label>
                    <input type="text" id="phn1" name="phn1" class="form-control" aria-required="true">
                  </div>
                </div>
                <div class="row">
                  <div class="col-12 form-group">
                    <label for="">Requirement</label>
                    <textarea class="form-control" id="msg1" name="msg1" cols="30" rows="10"></textarea>
                  </div>
                  <div class="col-12 text-left">
                    <button type="button" class="btn btn-yellow position-relative" id="form1" data-id="1" disabled>
                      <span>Contact Us</span>
                      <img class="load" width="100" src="<?php echo get_template_directory_uri(); ?>/assets/images/gif.gif">
                      <img class="suc d-none" width="25" src="<?php echo get_template_directory_uri(); ?>/assets/images/success.gif">
                    </button>
                    
                  </div>
                </div>
              </form>
            </div>
            <div class="tab-pane fade" id="car-type" role="tabpanel" aria-labelledby="car-type-tab">
              <form id="f2" method="POST" class="contact-form">
                <div class="row">
                  <div class="col form-group">
                    <label class="requird" for="">First name</label>
                    <input type="text" id="fn2" name="fn2" class="form-control" aria-required="true" onkeyup="validate2()">
                  </div>
                  <div class="col form-group">
                    <label class="requird" for="">Last name</label>
                    <input type="text" id="ln2" name="ln2" class="form-control" aria-required="true" onkeyup="validate2()">
                  </div>
                </div>
                <div class="row">
                  <div class="col-12 form-group">
                    <label class="requird" for="">Email</label>
                    <input type="email" id="eml2" name="eml2" class="form-control" aria-required="true" onkeyup="validate2()">
                  </div>
                </div>
                <div class="row">
                  <div class="col form-group">
                    <label class="requird" for="">Company</label>
                    <input type="text" id="com2" name="com2" class="form-control" aria-required="true" onkeyup="validate2()">
                  </div>
                  <div class="col form-group">
                    <label class="requird" for="">Type</label>
                    <input type="text" class="form-control" aria-required="true" placeholder="End Customer" disabled>
                    <input type="text" class="d-none" value="End Customer" id="typ2" name="typ2">
                  </div>
                </div>
                <div class="row">
                  <div class="col-12 form-group">
                    <label class="requird" for="">Product Group</label>
                    <select class="form-control" id="pro2" name="pro2" onchange="validate2()">
                      <option value="">Choose Product Group</option>
                      <?php foreach($search_contact_response_json->data as $pro_grp):
                      ?>
                      <option value="<?php echo $pro_grp->id; ?>"><?php echo $pro_grp->Name; ?></option>
                      <?php endforeach; ?>
                    </select>
                  </div>
                </div>
                <div class="row">
                  <div class="col form-group">
                    <label class="requird" for="">Mobile</label>
                    <input type="text" id="mob2" name="mob2" class="form-control" aria-required="true" onkeyup="validate2()">
                  </div>
                  <div class="col form-group">
                    <label for="">Phone</label>
                    <input type="text" id="phn2" name="phn2" class="form-control" aria-required="true">
                  </div>
                </div>
                <div class="row">
                  <div class="col-12 form-group">
                    <label for="">Requirement</label>
                    <textarea class="form-control" id="msg2" name="msg2" cols="30" rows="10"></textarea>
                  </div>
                  <div class="col-12 text-left">
                    <button type="button" class="btn btn-yellow position-relative" id="form2" data-id="2" disabled>
                      <span>Contact Us</span>
                      <img class="load" width="100" src="<?php echo get_template_directory_uri(); ?>/assets/images/gif.gif">
                      <img class="suc d-none" width="25" src="<?php echo get_template_directory_uri(); ?>/assets/images/success.gif">
                    </button>
                  </div>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
<script>
var ajaxurl = "<?php echo admin_url('admin-ajax.php'); ?>";
function validate() {
   var reg = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;
    if($("#fn1").val() != "" && $("#ln1").val() != "" && $("#eml1").val() != "" && $("#com1").val() != "" && $("#typ1").val() != "" && $("#pro1").val() != "" && $("#mob1").val() && reg.test($("#eml1").val()) == true) {
        $(".contact-section #form1").removeAttr("disabled");
    } else {
        $(".contact-section #form1").attr("disabled", "disabled");
    }
}
function validate2() {
   var reg = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;
    if($("#fn2").val() != "" && $("#ln2").val() != "" && $("#eml2").val() != "" && $("#com2").val() != "" && $("#typ2").val() != "" && $("#pro2").val() != "" && $("#mob2").val() && reg.test($("#eml2").val()) == true) {
        $(".contact-section #form2").removeAttr("disabled");
    } else {
        $(".contact-section #form2").attr("disabled", "disabled");
    }
}
</script>