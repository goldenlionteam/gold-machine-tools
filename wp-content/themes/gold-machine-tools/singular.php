<?php
/**
 * The template for displaying single posts and pages.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package WordPress
 * @subpackage Leaseacar
 */

get_header();
?>

<!--Content section-->
<?php get_template_part("template-parts/blog-details/content")?>

<!--Sidebar section-->
<?php get_template_part("template-parts/blog-details/sidebar")?>

<?php get_footer(); ?>
